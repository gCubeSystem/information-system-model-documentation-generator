
.. _Property:

Property
========


.. _GCubeProperty:

GCubeProperty
-------------

Marker type for any properties extended in the gCube model.


.. table:: **GCubeProperty** ``extends`` **Property**

	+------------------------------------------------------+------+------------+-------------+
	| Name                                                 | Type | Attributes | Description |
	+======================================================+======+============+=============+
	| This type does not define any additional attributes.                                   |
	+------------------------------------------------------+------+------------+-------------+


The **GCubeProperty** current version is 1.0.0.

Changelog:

* **1.0.0**: First Version.


.. _AccessPolicy:

AccessPolicy
^^^^^^^^^^^^

AccessPolicy information


.. table:: **AccessPolicy** ``extends`` **GCubeProperty**

	+--------+----------------------------------+----------------------------------------------------------+-------------+
	| Name   | Type                             | Attributes                                               | Description |
	+========+==================================+==========================================================+=============+
	| policy | :ref:`ValueSchema <ValueSchema>` | ``Mandatory:false`` ``ReadOnly:false`` ``NotNull:false`` |             |
	+--------+----------------------------------+----------------------------------------------------------+-------------+
	| note   | String                           | ``Mandatory:false`` ``ReadOnly:false`` ``NotNull:false`` |             |
	+--------+----------------------------------+----------------------------------------------------------+-------------+


The **AccessPolicy** current version is 1.0.0.

Changelog:

* **1.0.0**: First Version.


.. _AttributeProperty:

AttributeProperty
^^^^^^^^^^^^^^^^^

This class model as property any attrbute must be instantiated.


.. table:: **AttributeProperty** ``extends`` **GCubeProperty**

	+--------------+---------+----------------------------------------------------------+------------------------------------------+
	| Name         | Type    | Attributes                                               | Description                              |
	+==============+=========+==========================================================+==========================================+
	| description  | String  | ``Mandatory:true`` ``ReadOnly:false`` ``NotNull:true``   |                                          |
	+--------------+---------+----------------------------------------------------------+------------------------------------------+
	| max          | Integer | ``Mandatory:true`` ``ReadOnly:false`` ``NotNull:true``   |                                          |
	+--------------+---------+----------------------------------------------------------+------------------------------------------+
	| min          | Integer | ``Mandatory:true`` ``ReadOnly:false`` ``NotNull:true``   |                                          |
	+--------------+---------+----------------------------------------------------------+------------------------------------------+
	| name         | String  | ``Mandatory:true`` ``ReadOnly:true`` ``NotNull:true``    | The name of the Query Template Variable. |
	+--------------+---------+----------------------------------------------------------+------------------------------------------+
	| propertyType | String  | ``Mandatory:true`` ``ReadOnly:false`` ``NotNull:true``   |                                          |
	+--------------+---------+----------------------------------------------------------+------------------------------------------+
	| regexp       | String  | ``Mandatory:true`` ``ReadOnly:false`` ``NotNull:true``   |                                          |
	+--------------+---------+----------------------------------------------------------+------------------------------------------+
	| defaultValue | Any     | ``Mandatory:false`` ``ReadOnly:false`` ``NotNull:false`` |                                          |
	+--------------+---------+----------------------------------------------------------+------------------------------------------+


The **AttributeProperty** current version is 1.0.0.

Changelog:

* **1.0.0**: First Version.


.. _EnumStringProperty:

EnumStringProperty
^^^^^^^^^^^^^^^^^^

Enum String Property


.. table:: **EnumStringProperty** ``extends`` **GCubeProperty**

	+------------------------------------------------------+------+------------+-------------+
	| Name                                                 | Type | Attributes | Description |
	+======================================================+======+============+=============+
	| This type does not define any additional attributes.                                   |
	+------------------------------------------------------+------+------------+-------------+


The **EnumStringProperty** current version is 1.0.0.

Changelog:

* **1.0.0**: First Version.


.. _RegexProperty:

RegexProperty
^^^^^^^^^^^^^

A property validated with a regular expression.


.. table:: **RegexProperty** ``extends`` **GCubeProperty**

	+--------+--------+----------------------------------------------------------+-------------+
	| Name   | Type   | Attributes                                               | Description |
	+========+========+==========================================================+=============+
	| value  | String | ``Mandatory:false`` ``ReadOnly:false`` ``NotNull:false`` |             |
	+--------+--------+----------------------------------------------------------+-------------+
	| schema | String | ``Mandatory:false`` ``ReadOnly:false`` ``NotNull:false`` |             |
	+--------+--------+----------------------------------------------------------+-------------+


The **RegexProperty** current version is 1.0.0.

Changelog:

* **1.0.0**: First Version.


.. _ValueSchema:

ValueSchema
^^^^^^^^^^^

This type aims at exposing a value which can be automatically managed by any client with no knowledge of its format.


.. table:: **ValueSchema** ``extends`` **GCubeProperty**

	+--------+--------+----------------------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------+
	| Name   | Type   | Attributes                                               | Description                                                                                                                                                 |
	+========+========+==========================================================+=============================================================================================================================================================+
	| value  | String | ``Mandatory:true`` ``ReadOnly:false`` ``NotNull:true``   | The value which schema is available at the URI provided in the schema property.                                                                             |
	+--------+--------+----------------------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------+
	| schema | String | ``Mandatory:false`` ``ReadOnly:false`` ``NotNull:false`` | An URI containing a schema used to validate/interpret the content of the value. It is only an informative field. The validation is in charge of the client. |
	+--------+--------+----------------------------------------------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------+


The **ValueSchema** current version is 1.0.0.

Changelog:

* **1.0.0**: First Version.

