This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for Information System Model

## [v1.0.0-SNAPSHOT]

- First Release

