package org.gcube.informationsystem.utils.documentation.table;

import org.gcube.informationsystem.utils.documentation.rst.table.Cell;
import org.gcube.informationsystem.utils.documentation.rst.table.Row;
import org.gcube.informationsystem.utils.documentation.rst.table.RowType;
import org.gcube.informationsystem.utils.documentation.rst.table.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class TableTest {

	private static final Logger logger = LoggerFactory.getLogger(TableTest.class);
	
	// @Test
	public void test() throws Exception {
		Table table = new Table();
		
		Row row1 = new Row(RowType.HEADING);
		table.appendRow(row1);
		
		Cell cell11 = new Cell();
		cell11.setText("Column 1");
		row1.appendCell(cell11);
		
		Cell cell12 = new Cell();
		cell12.setText("Column 2");
		row1.appendCell(cell12);
		
		Cell cell13 = new Cell();
		cell13.setText("Column 3");
		row1.appendCell(cell13);
		
		Row row2 = new Row();
		table.appendRow(row2);
		
		Cell cell21 = new Cell();
		cell21.setText("Row 2 Content 1");
		row2.appendCell(cell21);
		
		Cell cell22 = new Cell();
		cell22.setText("Row 2 Content 2");
		row2.appendCell(cell22);
		
		Cell cell23 = new Cell();
		cell23.setText("Row 2 Content 3");
		row2.appendCell(cell23);
		
		
		Row row3 = new Row();
		table.appendRow(row3);
		
		Cell cell31 = new Cell();
		cell31.setText("Row 3 Content 31");
		row3.appendCell(cell31);
		
		Cell cell32 = new Cell();
		cell32.setText("Row 3 Content 32");
		row3.appendCell(cell32);
		
		Cell cell33 = new Cell();
		cell33.setText("Row 3 Content 33");
		row3.appendCell(cell33);
		
		StringBuffer stringBuffer = table.generateTable("Test 1");
		logger.info(stringBuffer.toString());
		
		cell11.setText("Longer Longer  Column 1");
		cell33.setText("Longer Longer Row 3 Content 33");
		
		stringBuffer = table.generateTable("Test 2");
		logger.info(stringBuffer.toString());
		
	}
	
}
