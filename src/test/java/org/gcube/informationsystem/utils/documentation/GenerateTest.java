package org.gcube.informationsystem.utils.documentation;

import java.util.ServiceLoader;

import org.gcube.informationsystem.discovery.ISModelRegistrationProvider;
import org.gcube.informationsystem.discovery.RegistrationProvider;
import org.gcube.informationsystem.discovery.knowledge.Knowledge;
import org.gcube.informationsystem.utils.documentation.generator.ModelKnowledgeGenerator;
import org.gcube.informationsystem.utils.documentation.model.Documentation;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class GenerateTest {
	
	public static Logger logger = LoggerFactory.getLogger(GenerateTest.class);

	@Test
	public void testDocGeneration() throws Exception {
		Documentation.setDefaultOffsetLevel(2);

		ServiceLoader<? extends RegistrationProvider> registrationProviders = ServiceLoader
				.load(RegistrationProvider.class);
		for(RegistrationProvider rp : registrationProviders) {
			Knowledge.getInstance().validateModelKnowledge(rp);
			logger.info("ALL TEST PASSED FOR '{}'", rp.getModelName());
			
			
			ModelKnowledgeGenerator treeGenerator = new ModelKnowledgeGenerator(rp);
			if(rp instanceof ISModelRegistrationProvider) {
				treeGenerator.setSplittedFiles(false);
				treeGenerator.setDesiredFileName("is-model");
			}else {
				treeGenerator.setSplittedFiles(true);
			}
			
			logger.info("GOING TO CREATE DOCUMENTATION FOR '{}'", rp.getModelName());
			treeGenerator.generate();
		}
	}
	
}
