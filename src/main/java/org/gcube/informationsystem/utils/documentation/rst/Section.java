package org.gcube.informationsystem.utils.documentation.rst;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class Section {

	public enum SectionType {
		HEADING_1(true, "#"),
		HEADING_2(true, "*"),
		HEADING_3(false, "="),
		HEADING_4(false, "-"),
		HEADING_5(false, "^"),
		HEADING_6(false, "\"");
		
		boolean overline;
		String separator;
		
		private SectionType(boolean overline, String separator) {
			this.overline = overline;
			this.separator = separator;
		}
	}
	
	protected SectionType sectionType;
	
	public Section() {
		this.sectionType = SectionType.HEADING_1;
	}
	
	public Section(SectionType sectionType) {
		this.sectionType = sectionType;
	}
	
	public SectionType getSectionType() {
		return sectionType;
	}

	public void setSectionType(SectionType sectionType) {
		this.sectionType = sectionType;
	}
	
	protected StringBuffer getSectionSeparation(String separator, int lenght) {
		StringBuffer stringBuffer = new StringBuffer();
		for(int i=0; i<lenght; i++) {
			stringBuffer.append(separator);
		}
		return stringBuffer;
	}
	
	public StringBuffer generate(String sectionTitle, boolean addSectionReference) {
		StringBuffer stringBuffer = new StringBuffer();
		if(addSectionReference) {
			stringBuffer.append(".. _");
			stringBuffer.append(sectionTitle);
			stringBuffer.append(":\n\n");
		}
		int lenght = sectionTitle.length();
		if(sectionType.overline) {
			stringBuffer.append(getSectionSeparation(sectionType.separator, lenght));
			stringBuffer.append("\n");
		}
		stringBuffer.append(sectionTitle);
		stringBuffer.append("\n");
		stringBuffer.append(getSectionSeparation(sectionType.separator, lenght));
		stringBuffer.append("\n\n");
		return stringBuffer;
	}
	
}
