package org.gcube.informationsystem.utils.documentation.model.relations;

import java.util.Set;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.properties.LinkedEntity;
import org.gcube.informationsystem.utils.documentation.rst.table.Table;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class IsRelatedToDocumentation extends RelationDocumentation {

	public IsRelatedToDocumentation(Type type) {
		super(type, AccessType.IS_RELATED_TO);
		int level = 1;
		if(type.getName().compareTo(requiredType.getName())!=0) {
			++level;
		}
		setLevel(level);
	}

	@Override
	protected Table getTable() {
		Table table = super.getTable();
		
		table.appendRow(getKnownUsageBoldRow());
		table.appendRow(getSourceTargetBoldRow());
		Set<LinkedEntity> resources = resourceKnowledge.getUsage(type.getName());
		addLinkedEntities(table, resources, NO_SPECIFIC_KNOWN_USAGE);
		
		return table;
	}

	
}
