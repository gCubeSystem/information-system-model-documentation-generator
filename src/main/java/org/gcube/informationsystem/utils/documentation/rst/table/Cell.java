package org.gcube.informationsystem.utils.documentation.rst.table;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class Cell {

	private Row row;
	
	private int size;
	private String text;
	
	/**
	 * Cell Position in the row
	 */
	private Integer cellPosition;
	
	public Cell() {
		this.size = 0;
		this.text = "";
		this.row = null;
		this.cellPosition = null;
	}
	
	public int getSize() {
		return size;
	}
	
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
		this.size = text.length();
		if(row!=null) {
			this.row.update(this);
		}
	}
	
	protected void setRow(Row row) {
		if(this.row==null) {
			this.row = row;
		}
	}
	
	protected Integer getCellPosition() {
		return cellPosition;
	}

	protected void setPosition(Integer cellPosition) {
		if(this.cellPosition==null) {
			this.cellPosition = cellPosition;
		}
	}

}
