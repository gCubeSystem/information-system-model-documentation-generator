package org.gcube.informationsystem.utils.documentation.model.entities;

import java.util.Set;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.properties.LinkedEntity;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;
import org.gcube.informationsystem.utils.documentation.rst.table.Table;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class FacetDocumentation extends EntityDocumentation {

	public FacetDocumentation(Type type) {
		super(type, AccessType.FACET);
		int level = 1;
		if(type.getName().compareTo(requiredType.getName())!=0) {
			++level;
		}
		setLevel(level);
	}

	@Override
	protected Table getTable() {
		Table table = new Table();
		table.appendRow(getEntityHeadingRow());
		Set<PropertyDefinition> properties = type.getProperties();
		table = addPropertyRows(table, properties);
		
		table.appendRow(getKnownUsageBoldRow());
		table.appendRow(getSourceTargetBoldRow());
		
		Set<LinkedEntity> usage = facetKnowledge.getUsage(type.getName());
		addLinkedEntities(table, usage, NO_SPECIFIC_KNOWN_USAGE);
		
		return table;
	}

	
}
