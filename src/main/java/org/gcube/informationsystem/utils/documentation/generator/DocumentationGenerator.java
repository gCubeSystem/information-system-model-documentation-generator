package org.gcube.informationsystem.utils.documentation.generator;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.discovery.knowledge.ModelKnowledge;
import org.gcube.informationsystem.discovery.knowledge.UsageKnowledge;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.utils.documentation.model.Documentation;
import org.gcube.informationsystem.utils.documentation.model.entities.EntityDocumentation;
import org.gcube.informationsystem.utils.documentation.model.entities.FacetDocumentation;
import org.gcube.informationsystem.utils.documentation.model.entities.ResourceDocumentation;
import org.gcube.informationsystem.utils.documentation.model.properties.PropertyDocumentation;
import org.gcube.informationsystem.utils.documentation.model.relations.ConsistsOfDocumentation;
import org.gcube.informationsystem.utils.documentation.model.relations.IsRelatedToDocumentation;
import org.gcube.informationsystem.utils.documentation.model.relations.RelationDocumentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class DocumentationGenerator {

	private static final Logger logger = LoggerFactory.getLogger(DocumentationGenerator.class);
	
	protected ModelKnowledge modelKnowledge;
	
	public DocumentationGenerator() {
		this.modelKnowledge = new ModelKnowledge();
	}
	
	public ModelKnowledge getModelKnowledge() {
		return modelKnowledge;
	}

	public Class<? extends Documentation> getDocumentationClass(AccessType accessType)
			throws Exception {
		Class<? extends Documentation> clz;
		switch (accessType) {
			case PROPERTY:
				clz = PropertyDocumentation.class;
				break;
	
			case ENTITY:
				clz = EntityDocumentation.class;
				break;
	
			case RESOURCE:
				clz = ResourceDocumentation.class;
				break;
	
			case FACET:
				clz = FacetDocumentation.class;
				break;
	
			case RELATION:
				clz = RelationDocumentation.class;
				break;
	
			case IS_RELATED_TO:
				clz = IsRelatedToDocumentation.class;
				break;
	
			case CONSISTS_OF:
				clz = ConsistsOfDocumentation.class;
				break;
	
			default:
				throw new Exception("List of types not recognized");
		}
		return clz;
	}
	
	protected Documentation getDocumentation(Type type) throws Exception {
		Documentation d = null;
		switch (type.getAccessType()) {
			case PROPERTY:
				d = new PropertyDocumentation(type);
				d.setLevel(0);
				break;
			
			case ENTITY:
				d = new EntityDocumentation(type);
				d.setLevel(0);
				break;
		
			case RESOURCE:
				d = new ResourceDocumentation(type);
				d.setLevel(1);
				break;
	
			case FACET:
				d = new FacetDocumentation(type);
				d.setLevel(1);
				break;
			
			case RELATION:
				d = new RelationDocumentation(type);
				d.setLevel(0);
				break;
			
			case IS_RELATED_TO:
				d = new IsRelatedToDocumentation(type);
				d.setLevel(1);
				break;
				
			case CONSISTS_OF:
				d = new ConsistsOfDocumentation(type);
				d.setLevel(1);
				break;
				
			default:
				throw new Exception("I'm not developed to manage the " + AccessType.class.getSimpleName() + " " + type.getName());
		}
		UsageKnowledge facetKnowledge = modelKnowledge.getUsageKnowledge(AccessType.FACET);
		d.setFacetKnowledge(facetKnowledge);
		UsageKnowledge resourceKnowledge = modelKnowledge.getUsageKnowledge(AccessType.RESOURCE);
		d.setResourceKnowledge(resourceKnowledge);
		return d;
	}
	
	protected File getFile(String fileName, boolean newFile) throws IOException {
		File file = new File(fileName);
		
		if(file.exists() && newFile) {
			file.delete();
		}

		if(!file.exists()) {
			logger.info("Going to create {}", file.getAbsolutePath());
			file.createNewFile();
		}
		
		return file;
	}
	
	protected void writeSectionOnly(Type type, File file,  Integer level) throws Exception {
		Documentation dg = getDocumentation(type);
		if(level!=null) {
			dg.setLevel(level);
		}
		StringBuffer sb = dg.generateSection(); 
		Files.write(file.toPath(), sb.toString().getBytes(), StandardOpenOption.APPEND);
	}
	
	protected void writeTypeToFile(Type type, File file, Integer level) throws Exception {
		Documentation dg = getDocumentation(type);
		if(level!=null) {
			dg.setLevel(level);
		}
		StringBuffer sb = dg.generate();
		Files.write(file.toPath(), sb.toString().getBytes(), StandardOpenOption.APPEND);
	}
	
	protected void writeTypeToFile(Type type, File file) throws Exception {
		writeTypeToFile(type, file, null);
	}
	
}
