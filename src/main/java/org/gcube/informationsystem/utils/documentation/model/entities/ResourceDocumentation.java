package org.gcube.informationsystem.utils.documentation.model.entities;

import java.util.Set;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.entities.ResourceType;
import org.gcube.informationsystem.types.reference.properties.LinkedEntity;
import org.gcube.informationsystem.utils.documentation.rst.table.Table;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class ResourceDocumentation extends EntityDocumentation {

	public static final String FACETS_TABLE_HEADING = "**Facets**";
	public static final String RESOURCE_RELATIONS_TABLE_HEADING = "**Resource Relations**";
	public static final String NO_SPECIFIC_FACETS = "No specific facets usage defined for this type.";
	public static final String NO_SPECIFIC_RESOURCE_RELATIONS = "No specific Resource relations defined for this type.";
	
	public ResourceDocumentation(Type type) {
		super(type,AccessType.RESOURCE);
		int level = 1;
		if(type.getName().compareTo(requiredType.getName())!=0) {
			++level;
		}
		setLevel(level);
	}
	
	@Override
	protected Table getTable() {
		Table table = new Table();
		table.appendRow(getSourceTargetHeadingRow());
		table.appendRow(getRowCellSpan(FACETS_TABLE_HEADING, requiredNumberOfColumns));
		ResourceType resourceType = (ResourceType) type;
		
		Set<LinkedEntity> facets = facetKnowledge.getUsage(resourceType.getName());
		addLinkedEntities(table, facets, NO_SPECIFIC_FACETS);
		table.appendRow(getRowCellSpan(RESOURCE_RELATIONS_TABLE_HEADING, requiredNumberOfColumns));
		
		Set<LinkedEntity> resources = resourceKnowledge.getUsage(resourceType.getName());
		addLinkedEntities(table, resources, NO_SPECIFIC_RESOURCE_RELATIONS);
		return table;
	}
	
}
