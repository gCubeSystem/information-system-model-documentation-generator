package org.gcube.informationsystem.utils.documentation.rst.table;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class Table {
	
	public static final String COLUMN_SEPARATOR_IN_SEPERATOR_ROW = "+";
	public static final String COLUMN_SEPARATOR_IN_CONTENT_ROW = "|";
	public static final String ROW_SEPARATOR = "-";
	public static final String ROW_SEPARATOR_HEADING = "=";
	
	private List<Row> rows;
	private List<Integer> maxSizePerCell;
	
	public Table() {
		rows = new ArrayList<>();
		maxSizePerCell = new ArrayList<>();
	}
	
	protected void updateMaxSizePerCell(Row row) {
		List<Integer> cellSizes = row.getCellSizes();
		for(int i=0; i<cellSizes.size(); i++) {
			Integer cellSize = cellSizes.get(i);
			cellSize = cellSize!=null ? cellSize : 0;
			cellSize += 2; // We need to consider one space before and one after to calculate total column size
			Integer maxSize = 0;
			try {
				maxSize = maxSizePerCell.get(i);
				if(cellSize>=maxSize) {
					maxSizePerCell.set(i, cellSize);
				}
			} catch (IndexOutOfBoundsException  e) {
				maxSizePerCell.add(i, cellSize);
			}
						
		}
	}
	
	public void appendRow(Row row) {
		rows.add(row);
		row.setRowPosition(rows.size()-1);
		row.setTable(this);
		updateMaxSizePerCell(row);
	}
	
	private StringBuffer addRowSeparator(String separator) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("\t");
		stringBuffer.append(COLUMN_SEPARATOR_IN_SEPERATOR_ROW);
		for(int i=0; i<maxSizePerCell.size(); i++) {
			for(int j=0; j<maxSizePerCell.get(i); j++) {
				stringBuffer.append(separator);
			}
			stringBuffer.append(COLUMN_SEPARATOR_IN_SEPERATOR_ROW);
		}
		stringBuffer.append("\n");
		return stringBuffer;
	}
	
	private StringBuffer addRowSeparator(Row row, String separator) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("\t");
		stringBuffer.append(COLUMN_SEPARATOR_IN_SEPERATOR_ROW);
		for(int i=0; i<maxSizePerCell.size(); i++) {
			for(int j=0; j<maxSizePerCell.get(i); j++) {
				stringBuffer.append(separator);
			}
			stringBuffer.append(COLUMN_SEPARATOR_IN_SEPERATOR_ROW);
		}
		stringBuffer.append("\n");
		return stringBuffer;
	}
	
	private StringBuffer addRow(Row row) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("\t");
		for(int i=0; i<maxSizePerCell.size(); i++) {
			Cell cell = row.getCell(i);
			int sizeTosatisfy = maxSizePerCell.get(i);
			if(i>0 && cell==row.getCell(i-1)) {
				for(int j=0; j<sizeTosatisfy+1; j++) {
					stringBuffer.append(" ");
				}
			}else {
				stringBuffer.append(COLUMN_SEPARATOR_IN_CONTENT_ROW);
				int remaining = sizeTosatisfy - cell.getSize();
				stringBuffer.append(" ");
				stringBuffer.append(cell.getText());
				for(int j=0; j<remaining-1; j++) {
					stringBuffer.append(" ");
				}
			}
		}
		stringBuffer.append(COLUMN_SEPARATOR_IN_CONTENT_ROW);
		stringBuffer.append("\n");
		return stringBuffer;
	}
	
	public StringBuffer generateTable(String title) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("\n\n");
		stringBuffer.append(".. table:: ");
		stringBuffer.append(title!=null?title:"");
		stringBuffer.append("\n\n");
		stringBuffer.append(addRowSeparator(RowType.NORMAL.getRowSeparator()));
		for(Row row : rows) {
			stringBuffer.append(addRow(row));
			stringBuffer.append(addRowSeparator(row, row.getRowType().getRowSeparator()));
		}
		stringBuffer.append("\n\n");
		return stringBuffer;
	}
	
}

