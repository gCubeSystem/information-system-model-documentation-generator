package org.gcube.informationsystem.utils.documentation.model.properties;

import java.util.HashSet;
import java.util.Set;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.properties.PropertyElement;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;
import org.gcube.informationsystem.utils.documentation.model.Documentation;
import org.gcube.informationsystem.utils.documentation.rst.table.Table;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class PropertyDocumentation extends Documentation {
	
	public PropertyDocumentation(Type type) {
		super(type, AccessType.PROPERTY);
		this.requiredNumberOfColumns = 4;
		this.superClassToBeExcluded = TypeMapper.getType(PropertyElement.class);
		int level = 0;
		if(type.getName().compareTo(requiredType.getName())!=0) {
			++level;
		}
		setLevel(level);
	}
	
	@Override
	protected Table getTable() {
		
		Table table = new Table();
		table.appendRow(getPropertyFieldsHeadingRow());
		Set<PropertyDefinition> properties = type.getProperties();
		if(properties!=null && properties.size()>0) {
			Set<PropertyDefinition> notMandatoryProperties = new HashSet<>();
			for(PropertyDefinition propertyDefinition : properties) {
				if(propertyDefinition.isMandatory()) {
					/*
					 * Adding Mandatory properties first in the table
					 */
					table.appendRow(getPropertyRow(propertyDefinition));
				}else {
					notMandatoryProperties.add(propertyDefinition);
				}
			}
			for(PropertyDefinition propertyDefinition : notMandatoryProperties) {
				table.appendRow(getPropertyRow(propertyDefinition));
			}
		}else {
			table.appendRow(getNoPropertyRow());
		}
		return table;
	}
	

}
