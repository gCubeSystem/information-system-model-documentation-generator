package org.gcube.informationsystem.utils.documentation.model.entities;

import java.util.HashSet;
import java.util.Set;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.entities.EntityElement;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;
import org.gcube.informationsystem.utils.documentation.model.Documentation;
import org.gcube.informationsystem.utils.documentation.rst.table.Row;
import org.gcube.informationsystem.utils.documentation.rst.table.RowType;
import org.gcube.informationsystem.utils.documentation.rst.table.Table;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class EntityDocumentation extends Documentation {

	public EntityDocumentation(Type type) {
		super(type, AccessType.ENTITY);
		this.requiredNumberOfColumns = 4;
		this.superClassToBeExcluded = TypeMapper.getType(EntityElement.class);
		setLevel(0);
	}
	
	protected EntityDocumentation(Type type, AccessType requiredType) {
		super(type, requiredType);
	}
	
	protected Row getEntityHeadingRow() {
		Row row = new Row(RowType.HEADING);
		row = addCellSpanToRow(row, "Properties", requiredNumberOfColumns);
		return row;
	}
	
	@Override
	protected Table getTable() {
		Type entityElementType = TypeMapper.createTypeDefinition(EntityElement.class); 
		Table table = new Table();
		table.appendRow(getEntityHeadingRow());
		table.appendRow(getPropertyFieldsBoldRow());
		Set<PropertyDefinition> properties = entityElementType.getProperties();
		if(properties!=null && properties.size()>0) {
			Set<PropertyDefinition> notMandatoryProperties = new HashSet<>();
			for(PropertyDefinition propertyDefinition : properties) {
				if(propertyDefinition.isMandatory()) {
					/*
					 * Adding Mandatory properties first in the table
					 */
					table.appendRow(getPropertyRow(propertyDefinition));
				}else {
					notMandatoryProperties.add(propertyDefinition);
				}
			}
			for(PropertyDefinition propertyDefinition : notMandatoryProperties) {
				table.appendRow(getPropertyRow(propertyDefinition));
			}
		}else {
			table.appendRow(getNoPropertyRow());
		}
		return table;
	}

	
}
