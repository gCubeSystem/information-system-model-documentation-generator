package org.gcube.informationsystem.utils.documentation.rst.table;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class Row {

	private RowType rowType;
	
	private Table table;
	/**
	 * Row Position in the table
	 */
	private Integer rowPosition;
	
	private List<Cell> cells;
	private List<Integer> cellSizes;
	
	public Row() {
		this.rowType = RowType.NORMAL;
		this.cells = new ArrayList<>();
		this.cellSizes = new ArrayList<>();
	}
	
	public Row(RowType rowType) {
		this.rowType = rowType;
		this.cells = new ArrayList<>();
		this.cellSizes = new ArrayList<>();
	}
	
	public void setRowType(RowType rowType) {
		this.rowType = rowType;
	}
	
	public RowType getRowType() {
		return rowType;
	}
	
//	public void update() {
//		for(int position=0; position<cells.size(); position++) {
//			Integer size = cells.get(position).getSize();
//			if(position>0 && cells.get(position)==cells.get(position-1)) {
//				size=null;
//			}
//			cellSizes.add(position, size);
//		}
//	}
	
	public void update(Cell cell) {
		Integer size = cell.getSize();
		int cellPosition = cell.getCellPosition();
		if(cellSizes.size()-1<cellPosition) {
			cellSizes.add(cellPosition, size);
		}else {
			cellSizes.set(cellPosition, size);
		}
		if(table!=null) {
			table.updateMaxSizePerCell(this);
		}
	}
	
	public void appendCell(Cell cell) {
		int position = cells.size();
		setCell(position, cell);
	}
	
	protected void setCell(int position, Cell cell) {
		if(position>cells.size()) {
			throw new RuntimeException("You can't add the cell in position " + position);
		}
		cells.add(position, cell);
		if(position==0 || cell!=cells.get(position-1)) {
			cell.setRow(this);
			cell.setPosition(position);
			update(cell);
		}else {
			cellSizes.add(position, 0);
		}
		
	}
	
	public List<Cell> getCells() {
		return cells;
	}

	public Cell getCell(int position) {
		try {
			return cells.get(position);
		}catch (IndexOutOfBoundsException e) {
			return null;
		}
	}
	
	public List<Integer> getCellSizes() {
		return cellSizes;
	}
	
	public Integer getCellSize(int position) {
		try {
			return cellSizes.get(position);
		}catch (IndexOutOfBoundsException e) {
			return null;
		}	
	}

	protected void setTable(Table table) {
		if(this.table==null) {
			this.table = table;
		}
	}

	protected Integer getRowPosition() {
		return rowPosition;
	}

	protected void setRowPosition(Integer rowPosition) {
		this.rowPosition = rowPosition;
	}
}
