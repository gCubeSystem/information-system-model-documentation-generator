package org.gcube.informationsystem.utils.documentation.rst.table;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public enum RowType {
	HEADING(Table.ROW_SEPARATOR_HEADING),
	NORMAL(Table.ROW_SEPARATOR);
	
	protected String rowSeparator;
	
	private RowType(String rowSeparator) {
		this.rowSeparator = rowSeparator;
	}
	
	public String getRowSeparator() {
		return rowSeparator;
	}
}