package org.gcube.informationsystem.utils.documentation.generator;

import java.io.File;
import java.util.Collection;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.Element;
import org.gcube.informationsystem.discovery.ISModelRegistrationProvider;
import org.gcube.informationsystem.discovery.RegistrationProvider;
import org.gcube.informationsystem.discovery.knowledge.Knowledge;
import org.gcube.informationsystem.model.reference.entities.Facet;
import org.gcube.informationsystem.model.reference.entities.Resource;
import org.gcube.informationsystem.model.reference.relations.ConsistsOf;
import org.gcube.informationsystem.model.reference.relations.IsRelatedTo;
import org.gcube.informationsystem.tree.Node;
import org.gcube.informationsystem.tree.NodeElaborator;
import org.gcube.informationsystem.tree.Tree;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Generate the documentation creating a ModelKnowledge
 * for the specified RegistrationProvider
 * 
 * @author Luca Frosini (ISTI - CNR)
 */
public class ModelKnowledgeGenerator extends DocumentationGenerator {

	private static final Logger logger = LoggerFactory.getLogger(ModelKnowledgeGenerator.class);
	
	public static final String EXTENSION = ".rst";
	public static final String FILENAME_SEPARATOR = "-";
	
	protected RegistrationProvider rp;
	protected boolean splittedFiles;
	protected String desiredFileName;

	public ModelKnowledgeGenerator(RegistrationProvider rp) throws Exception {
		super();
		this.rp = rp;
		this.modelKnowledge = Knowledge.getInstance().getModelKnowledge(rp);
		this.splittedFiles = false;
		this.desiredFileName = rp.getModelName().toLowerCase().replaceAll(" ", FILENAME_SEPARATOR);
	}
	
	public void setSplittedFiles(boolean splittedFiles) {
		if(rp instanceof ISModelRegistrationProvider && splittedFiles) {
			throw new RuntimeException(rp.getModelName() + " documentation can be only produced in a single file");
		}
		this.splittedFiles = splittedFiles;
	}

	public void setDesiredFileName(String desiredFileName) {
		this.desiredFileName = desiredFileName;
	}	
	
	public void elaborateTree(final AccessType at, Tree<Class<Element>> tree, File file, RegistrationProvider rp) throws Exception {
		logger.debug("Going to elaborate the following type tree\n{}", tree.toString());
		
		Collection<Package> packages = rp.getPackagesToRegister();
		
		NodeElaborator<Class<Element>> documentationNE = new NodeElaborator<Class<Element>>() {
			
			@Override
			public void elaborate(Node<Class<Element>> node, int level) throws Exception {
				Class<Element> clz = node.getNodeElement(); 
				if(level!=0 && !packages.contains(clz.getPackage())) {
					logger.debug("Discarding {} because it does not belong to {}", clz.getSimpleName(), rp.getModelName());
					return;
				}
				Type type = TypeMapper.createTypeDefinition(clz);
				if(level==0 && !(rp instanceof ISModelRegistrationProvider)) {
					/*
					 * Root node has been already documented in IS_MODEL_FILENAME
					 * Going to skip it
					 */
					writeSectionOnly(type, file, level);
				} else {
					if(rp instanceof ISModelRegistrationProvider) {
						switch (type.getName()) {
							case Resource.NAME:
							case Facet.NAME:
							case IsRelatedTo.NAME:
							case ConsistsOf.NAME:
								level++;
								break;
	
							default:
								break;
						}
					}
					writeTypeToFile(type, file, level);
				}
				
			}

		};
		
		tree.elaborate(documentationNE);
	}
	
	protected String getFileName(AccessType accessType) {
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append(desiredFileName);
		if(accessType!=null && splittedFiles) {
			stringBuffer.append(FILENAME_SEPARATOR);
			stringBuffer.append(accessType.getName().toLowerCase());
		}
		stringBuffer.append(EXTENSION);
		return stringBuffer.toString();
	}
	
	public void generate() throws Exception {
		
		File file = null;
		
		if(!splittedFiles) {
			file = getFile(getFileName(null), true);
		}
		
		for(AccessType at : AccessType.getModelTypes()) {
			if(splittedFiles) {
				file = getFile(getFileName(at), true);
			}
			
			switch (at) {
				case PROPERTY:
					break;
				
				case RESOURCE:
					if(rp instanceof ISModelRegistrationProvider) {
						Type type = TypeMapper.createTypeDefinition(AccessType.ENTITY.getTypeClass());
						writeTypeToFile(type, file);
					}
					break;
				
				case IS_RELATED_TO:
					if(rp instanceof ISModelRegistrationProvider) {
						Type type = TypeMapper.createTypeDefinition(AccessType.RELATION.getTypeClass());
						writeTypeToFile(type, file);
					}
					break;
				
				default:
					break;
			}
			
			Tree<Class<Element>> tree = modelKnowledge.getClassesTree(at);
			elaborateTree(at, tree, file, rp);
						
		}
		
	}
	
}
