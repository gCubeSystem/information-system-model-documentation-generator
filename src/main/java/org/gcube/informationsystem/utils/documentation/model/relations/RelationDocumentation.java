package org.gcube.informationsystem.utils.documentation.model.relations;

import java.util.Set;

import org.gcube.informationsystem.base.reference.AccessType;
import org.gcube.informationsystem.base.reference.relations.RelationElement;
import org.gcube.informationsystem.types.TypeMapper;
import org.gcube.informationsystem.types.reference.Type;
import org.gcube.informationsystem.types.reference.entities.EntityType;
import org.gcube.informationsystem.types.reference.properties.PropertyDefinition;
import org.gcube.informationsystem.types.reference.relations.RelationType;
import org.gcube.informationsystem.utils.documentation.model.Documentation;
import org.gcube.informationsystem.utils.documentation.rst.table.Cell;
import org.gcube.informationsystem.utils.documentation.rst.table.Row;
import org.gcube.informationsystem.utils.documentation.rst.table.RowType;
import org.gcube.informationsystem.utils.documentation.rst.table.Table;

/**
 * @author Luca Frosini (ISTI - CNR)
 */
public class RelationDocumentation extends Documentation {

	public RelationDocumentation(Type type) {
		super(type, AccessType.RELATION);
		this.superClassToBeExcluded = TypeMapper.getType(RelationElement.class);
		setLevel(0);
	}
	
	protected RelationDocumentation(Type type, AccessType requiredType) {
		super(type, requiredType);
	}

	protected Row getTypeDefinitionRow(RelationType<? extends EntityType, ? extends EntityType> relationType) {
		Row row = new Row(RowType.NORMAL);
		Cell source = new Cell();
		source.setText(getReference(relationType.getSource().getName()));
		row.appendCell(source);
		
		Cell relation = new Cell();
		relation.setText(getReference(relationType.getName()));
		row.appendCell(relation);
		
		Cell multiplicity = new Cell();
		multiplicity.setText("0..n");
		row.appendCell(multiplicity);
		
		Cell target = new Cell();
		target.setText(getReference(relationType.getTarget().getName()));
		row.appendCell(target);
		
		Cell description = new Cell();
		description.setText(checkDescription(relationType.getDescription()));
		row.appendCell(description);
		
		for(int i=row.getCells().size(); i<requiredNumberOfColumns; i++) {
			row.appendCell(description);
		}
		
		return row;
	}
	
	@Override
	protected Table getTable() {
		Table table = new Table();
		table.appendRow(getHeadingRowCellSpan("**Definition**", requiredNumberOfColumns));
		
		table.appendRow(getSourceTargetBoldRow());
		@SuppressWarnings("unchecked")
		RelationType<? extends EntityType, ? extends EntityType> relationType = (RelationType<? extends EntityType, ? extends EntityType>) type;
		table.appendRow(getTypeDefinitionRow(relationType));
		
		table.appendRow(getRowCellSpan("**Properties**", requiredNumberOfColumns));
		Set<PropertyDefinition> properties = type.getProperties();
		table = addPropertyRows(table, properties);
		
		return table;
	}

	
}
